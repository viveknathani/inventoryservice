# HelpingHand - Inventory

This is the Inventory microservice for the project HelpingHand.

## API docs

Find the API documentation over [here](https://helpinghand-inventory.netlify.app/).
 
## tech 

- Node.js 
- TypeScript  
- PostgreSQL

## local setup 

Create a database `inventory` either through the terminal or the pgAdmin interface. 

Make the .env file using [example .env file](./.env.example). 

```bash
    npm install
    npm start
``` 
## contributing 

Refer to the [contributing guidelines](./docs/CONTRIBUTING.md) if you are interested in working on this project. 

## license 

[LICENSE](./LICENSE.md) 
