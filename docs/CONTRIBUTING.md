# CONTRIBUTING

## Basic steps 
1. Fork the repository. 
2. Clone your fork into your system. 
3. Work on your changes and push. 
4. Make a PR and wait for us to respond. 

## Structure of a commit

A good commit message is broken down into three parts:

```
> Top

> Middle

> Bottom
```
with *exactly* one-line spacing vertically between sections.

<br>
 
The *top* should follow the format:
```
<type>: <summary or the subject line> 
```

where *type* is one of:

    feat: The new feature you're adding to a particular application
    fix: A bug fix
    style: Feature and updates related to styling
    refactor: Refactoring a specific section of the codebase
    test: Everything related to testing
    docs: Everything related to documentation
    chore: Regular code maintenance

**The net length of the top should not be more than 50 characters.**

Do not end the subject line with a period.
<br>

The *middle* (or the body):
```
Elaborate your work. Do not assume the reviewer understands what the original problem was, ensure you add it. Do not think your code is self-explanatory.
```

The *bottom* section is optional. Use it if you want to link the commit to an issue.
<br>
<hr> 
