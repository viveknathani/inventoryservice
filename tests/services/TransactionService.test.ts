import * as dotenv from 'dotenv';
dotenv.config();

import TransactionService from '../../src/services/TransactionService';
import { test_data } from '../data/transaction.json';

const transaction: TransactionService = new TransactionService();

test('insert/select/delete', async () => {

    try {
        // Set connection
        await TransactionService.init();

        // Run through the test data
        for (let i = 0; i < test_data.length; ++i) {

            // Parse and store
            let parsed = JSON.parse(JSON.stringify(test_data[i]));

            // Insert
            let insertedData = await transaction.addTransaction({...parsed});

            // Collect generated uuid
            let id = insertedData.dataValues.id;

            // Run a 'select' query
            let fetchedData = await transaction.getTransaction(id);

            // Compare returned data with inserted data
            expect(fetchedData.dataValues).toEqual(insertedData.dataValues);

            // Fetch data for a given bankId, should return > 0 rows
            let thisDonor = test_data[i].from;
            let allData = await transaction.getTransactions(test_data[i].donorType, thisDonor);
            expect(allData.length).toBeGreaterThanOrEqual(1);    
        }

        // Run a 'select' query on the whole table, should return > 0 rows
        let allData = await transaction.getAllTransactions();
        expect(allData.length).toBeGreaterThanOrEqual(1);

        // Goodbye
        await TransactionService.kill();
    }
    catch (err) {
        console.error(err);
        fail();
    }
});
