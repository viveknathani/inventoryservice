import * as dotenv from 'dotenv';
dotenv.config();

import InventoryService from '../../src/services/InventoryService';
import BankService from '../../src/services/BankService';
import { test_data } from '../data/inventory.json';

const inventory: InventoryService = new InventoryService();
const bank: BankService = new BankService();

test('insert/select/update/delete', async () => {

    try {
        // Set connection
        await InventoryService.init();

        // Insert a sample bank
        let bankData = await bank.addBank({
            name: 'abc',
            address: 'abc',
            district: 'abc',
            state: 'abc',
            email: 'abc@gmai.com',
            ownerId: 'QOa0ibg3zl9LSHz',
            pincode: 380005,
            mobileNumber: 1234567089
        });

        // Get the bank's id to be used as foreign key below
        let commonBankId = bankData.dataValues.id;;


        // Run through test data
        for (let i = 0; i < test_data.length; ++i) {

            // Parse and store
            let parsed = JSON.parse(JSON.stringify(test_data[i]));

            // Use common bankId
            parsed.bankId = commonBankId;

            // Insert
            let insertedData = await inventory.addItem({...parsed});

            // Collect the generated uuid
            let id = insertedData.dataValues.id;

             // Update the inserted data
            let updateData = await inventory.updateItem(id, { status: 'active'});

            // Count of affected rows
            expect(JSON.stringify(updateData)).toEqual("[1]");

            // Query with a hardcoded pair of objects
            let nestedData = await inventory.findWithTwoSets({ status: 'active'}, {
                name: 'abc',
                address: 'abc',
                district: 'abc',
                state: 'abc'
            });
            console.log(JSON.stringify(nestedData, null, 2));

            // Delete
            let deleteData = await inventory.deleteItem(id);

            // Count of affected rows
            expect(JSON.stringify(deleteData)).toEqual("1");

            // 'select' again, should be empty since whatever was inserted is now deleted
            let allData = await inventory.getAllItems(test_data[i].bankId);
            expect(allData.length).toEqual(0);
        }

        // Goodbye!
        await InventoryService.kill();
    }
    catch (err) {
        console.error(err);
        fail();
    }
});
