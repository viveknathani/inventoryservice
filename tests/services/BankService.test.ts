import * as dotenv from 'dotenv';
dotenv.config();

import BankService from '../../src/services/BankService';
import { test_data } from '../data/banks.json';

const bank: BankService = new BankService();

test('insert/select/update/delete', async () => {

    try {
        // Set connection
        await BankService.init();

        // Run through test data
        for (let i = 0; i < test_data.length; ++i) {

            // Parse and store
            let parsed = JSON.parse(JSON.stringify(test_data[i]));

            // Insert
            let insertedData = await bank.addBank({...parsed});

            // Collect the generated uuid
            let id = insertedData.dataValues.id;

            // Run a 'select' query
            let fetchedData = await bank.getBank(id);

            // Compare inserted data with previous query's result
            expect(fetchedData[0].dataValues).toEqual(insertedData.dataValues);

            // Update the inserted data
            let updateData = await bank.updateBank(id, { name: 'happy' });

            // 'select' again
            let fetchUpdated = await bank.getBank(id);

            // Compare originally inserted data with updated data
            expect(fetchUpdated[0].dataValues).not.toEqual(insertedData.dataValues);

            // Run a 'delete' query
            let deleteBank = await bank.deleteBank(id);

            // Should be 1, the number of destroyed rows.
            expect(JSON.stringify(deleteBank)).toEqual("1");

            // 'select' again
            fetchedData = await bank.getBank(id);   

            // Should be 0, the number of destroyed rows.
            expect(fetchedData.length).toEqual(0);
        }

        // Attempt to get all data
        let allData = await bank.getAllBanks();

        // Should be 0 or 1 since InventoryService.test.ts might add a bank later
        // Rest all banks are deleted.
        expect(allData.length).toBeLessThanOrEqual(1);

        // Goodbye!
        await BankService.kill();
    }
    catch (err) {
        console.error(err);
        fail();
    }
});
