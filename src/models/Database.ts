/**
 * Any database usage class should extend this class first.
 */
abstract class Database {

    abstract getBankModel():any;
    abstract getInventoryModel(): any;
    abstract getTransactionModel(): any;
    abstract setupConnection(): Promise<void>;
}

export default Database;