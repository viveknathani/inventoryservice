import config from '../config';
import Database from './Database';
import { Model, ModelCtor, Sequelize } from 'sequelize';
import * as schema from './schema';

/**
 * SQL based implementation of the database.
 */
class SQL extends Database {

    // Data members
    private sequelize: Sequelize; 
    private InventoryModel: ModelCtor<Model<any, any>>;
    private BankModel: ModelCtor<Model<any, any>>;
    private TransactionModel: ModelCtor<Model<any, any>>;
    private setConnection: boolean = false;    

    /**
     * 1. Initializes an instance of Sequelize, which basically means
     *    it makes a connection to the database.
     * 2. Sets up all the models using the pre-defined schema.
     */
    constructor() {
        
        super();
        this.sequelize = new Sequelize(config.database.name, 
                                       config.database.username, 
                                       config.database.password, {
                                        host: 'localhost',
                                        dialect: 'postgres',
                                        logging: false
                                    });

        this.InventoryModel = this.sequelize.define('Inventory', schema.InventorySchema, {
            tableName: 'bloodInventory'
        });

        this.BankModel = this.sequelize.define('Bank', schema.BankSchema, {
            tableName: 'bloodBank'
        });

        this.TransactionModel = this.sequelize.define('Transaction', schema.TransactionSchema, {
            tableName: 'bloodTransactions',
            timestamps: true,
            updatedAt: false            
        });

        this.BankModel.hasMany(this.InventoryModel, {
            foreignKey: 'bankId',
            onDelete: 'cascade'
        });

        this.InventoryModel.belongsTo(this.BankModel, {
            foreignKey: 'bankId'
        });
    }

    /**
     * This implementation actually 'tests' the connection, the success of which
     * implies that the application is connected to the database. Additionally, it
     * also synchronizes the models that are defined in the constructor.
     */
    async setupConnection() : Promise<void> {
        try {
            await this.sequelize.authenticate();
            await this.sequelize.sync();
            this.setConnection = true;
            console.log('Connected to the database.');
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * @returns The inventory model.
     */
    getInventoryModel(): ModelCtor<Model<any, any>> {
        return this.InventoryModel;
    }

    /**
     * @returns The bank model.
     */
    getBankModel(): ModelCtor<Model<any, any>> {
        return this.BankModel;
    }

    /**
     * @returns The transaction model.
     */
    getTransactionModel(): ModelCtor<Model<any, any>> {
        return this.TransactionModel;
    }
    
    /**
     * Check if connection is set.
     */
    isConnectionSet(): boolean {
        return this.setConnection;
    }

    async closeConnection(): Promise<void> {
        try {
            await this.sequelize.close();
        }
        catch (err) {
            console.error(err);
        }
    }
}

export default SQL;
