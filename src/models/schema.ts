import { DataTypes, ModelAttributes } from 'sequelize'

const InventorySchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },

    bloodGroup: {
        type: DataTypes.STRING,
        allowNull: false
    },

    donorId: {
        type: DataTypes.STRING,
        allowNull: false
    },

    expiryDate: {
        type: DataTypes.DATE,
        allowNull: false
    },

    bankId: {
        type: DataTypes.UUID,
        allowNull: false
    },

    status: {
        type: DataTypes.STRING,
        allowNull: false
    }
}

const BankSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },

    district: {
        type: DataTypes.STRING,
        allowNull: false
    },

    state: {
        type: DataTypes.STRING,
        allowNull: false
    },

    pincode: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    mobileNumber: {
        type: DataTypes.BIGINT,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    ownerId: {
        type: DataTypes.STRING,
        allowNull: false
    }
}

const TransactionSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },

    from: {
        type: DataTypes.STRING,
        allowNull: false
    },

    donorType: {
        type: DataTypes.STRING,
        allowNull: false
    },

    to: {
        type: DataTypes.STRING,
        allowNull: false
    },

    receiverType: {
        type: DataTypes.STRING,
        allowNull: false
    },

    quantity: {
        type: DataTypes.REAL,
        allowNull: false
    },

    comment: {
        type: DataTypes.TEXT,
        allowNull: false
    }
}

export {
    InventorySchema,
    BankSchema,
    TransactionSchema
}
