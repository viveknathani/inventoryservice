import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import config from './config';
import cors from 'cors';
import v1 from './api/v1';

const app: express.Application = express();
const PORT = config.PORT;

app.use(cors());
app.use(express.json());
app.use('/api/v1/', v1);
app.listen(PORT, () => console.log('Inventory server is up!'));
