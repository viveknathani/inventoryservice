import express, { Router } from 'express';
import bankRouter from './bank';
import inventoryRouter from './inventory';
import transactionRouter from './transaction';
import generalRouter from './general';

const router: Router = express.Router();

// Mount routes of all services
router.use('/', generalRouter);
router.use('/bank', bankRouter);
router.use('/inventory', inventoryRouter);
router.use('/transaction', transactionRouter);

export default router;
