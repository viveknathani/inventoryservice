import express, { NextFunction, Router } from 'express';
import { errorMessage400, errorMessage404, errorMessage5XX } from '../util';
import InventoryService from '../../services/InventoryService';

const generalRouter: Router = express.Router();
const inventoryInstance: InventoryService = new InventoryService();

generalRouter.use('/', async (req: express.Request, res: express.Response, next: NextFunction) => {
    try {
        await InventoryService.init();
        next();        
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {post} /find/blood/ Search bank data along with inventories nested.
 * @apiName General
 * @apiGroup General
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} [bankObject.name] Name of the bank
 * @apiParam {String} [bankObject.address] Address of the bank
 * @apiParam {String} [bankObject.district] District of the bank
 * @apiParam {String} [pincode] Stringified pincode of the bank
 * @apiParam {String} [bankObject.mobileNumber] Stringified mobile number of the bank
 * @apiParam {String} [bankObject.email] Email-id of the bank
 * @apiParam {String} [bankObject.ownerId] id of the owner of the bank
 * @apiParam {String} [inventoryObject.bloodGroup] Donor's blood group
 * @apiParam {String} [inventoryObject.donorId] Donor's id 
 * @apiParam {String} [inventoryObject.expiryDate] Mention the expiry date
 * @apiParam {String} [inventoryObject.status] Describe the status of inventory
 * @apiSuccessExample 
 * HTTP/1.1 200
 * [
 *      {
 *        "id": "1c3476e1-e9ba-4a4c-aeaa-e117616ec00e",
 *        "name": "abc",
 *        "address": "abc",
 *        "district": "abc",
 *        "state": "abc",
 *        "pincode": 380005,
 *        "mobileNumber": "1234567089",
 *        "email": "abc@gmai.com",
 *        "ownerId": "QOa0ibg3zl9LSHz",
 *        "createdAt": "2021-07-04T13:05:54.282Z",
 *        "updatedAt": "2021-07-04T13:05:54.282Z",
 *        "Inventories": [
 *          {
 *            "id": "76d09de5-6802-40a9-ad8a-7df0e6a51c53",
 *            "bloodGroup": "B+",
 *            "donorId": "Cf4ee24Eb18F66e",
 *            "expiryDate": "2020-06-07T00:00:00.000Z",
 *            "bankId": "1c3476e1-e9ba-4a4c-aeaa-e117616ec00e",
 *            "status": "active",
 *            "createdAt": "2021-07-04T13:05:54.496Z",
 *            "updatedAt": "2021-07-04T13:05:54.499Z"
 *          }
 *        ]
 *      }
 *    ]
 * @apiDescription The body of your request should be of JSON format. 
 * It should have two properties, bankObject and inventoryObject. The value of these two
 * properties contains the mentioned optional paramaters. An example would be,
 * {"bankObject": {"state": "abc"}, "inventoryObject": {"status": "active"}}
 * This example helps you find banks which are in "state"="abc" and their inventory contains
 * data whose "status"="active".
 */
generalRouter.post('/find/blood', async (req: express.Request, res: express.Response) => {
    try {
        console.log(req.body);
        const data: Promise<any> = await inventoryInstance.findWithTwoSets(req.body.inventoryObject, req.body.bankObject);

        if (data === null || data === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (JSON.stringify(data) === "[]") {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(data);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

export default generalRouter;
