import express, { NextFunction, Router } from 'express';
import TransactionService from '../../services/TransactionService';
import { errorMessage400, errorMessage404, errorMessage5XX } from '../util';

const transactionRouter: Router = express.Router();
const transactionInstance: TransactionService = new TransactionService();

transactionRouter.use('/', async (req: express.Request, res: express.Response, next: NextFunction) => {
    try {
        await TransactionService.init();
        next();
    }
    catch (err) {
        console.error(err);
        res.status(500).send({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/transaction/ Get all transactions
 * @apiName Transactions
 * @apiGroup Transactions
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *              "id": "20624250-c25b-4071-b120-22796346d564",
 *              "from": "c575c83e-fedb-43e9-8acd-9053c50964cb",
 *              "donorType": "bank",
 *              "to": "cd22cf55-bf43-40bf-8afc-3f48945a9a6b",
 *              "receiverType": "patient",
 *              "quantity": 36.9549,
 *              "comment": "some comment",
 *              "createdAt": "2021-06-30T12:07:45.714Z"
 *          }
 *      ]
 */
transactionRouter.get('/', async (req: express.Request, res: express.Response) => {
   try {
       const transactions: Promise<any> = await transactionInstance.getAllTransactions();
       res.status(200).json(transactions);
   } 
   catch (err) {
       console.error(err);
       res.status(500).send({ message: errorMessage5XX });
   }
});

/**
 * @api {get} /api/v1/transaction/bank/:bankId Get all transactions for a given bank
 * @apiName Transactions
 * @apiGroup Transactions
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *              "id": "20624250-c25b-4071-b120-22796346d564",
 *              "from": "c575c83e-fedb-43e9-8acd-9053c50964cb",
 *              "donorType": "bank",
 *              "to": "cd22cf55-bf43-40bf-8afc-3f48945a9a6b",
 *              "receiverType": "patient",
 *              "quantity": 36.9549,
 *              "comment": "some comment",
 *              "createdAt": "2021-06-30T12:07:45.714Z"
 *          }
 *      ]
 */
transactionRouter.get('/bank/:bankId', async (req: express.Request, res: express.Response) => {
    try {
        const transactions: Promise<any> = await transactionInstance.getTransactions('bank', req.params.bankId);

        if (transactions === null || transactions === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (JSON.stringify(transactions) === "[]") {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(transactions);
    } 
    catch (err) {
        console.error(err);
        res.status(500).send({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/transaction/donor/:donorId Get all transactions for a given donor
 * @apiName Transactions
 * @apiGroup Transactions
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *              "id": "20624250-c25b-4071-b120-22796346d564",
 *              "from": "c575c83e-fedb-43e9-8acd-9053c50964cb",
 *              "donorType": "donor",
 *              "to": "cd22cf55-bf43-40bf-8afc-3f48945a9a6b",
 *              "receiverType": "patient",
 *              "quantity": 36.9549,
 *              "comment": "some comment",
 *              "createdAt": "2021-06-30T12:07:45.714Z"
 *          }
 *      ]
 */
transactionRouter.get('/donor/:donorId', async (req: express.Request, res: express.Response) => {
    try {
        const transactions: Promise<any> = await transactionInstance.getTransactions('donor', req.params.donorId);

        if (transactions === null || transactions === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (JSON.stringify(transactions) === "[]") {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(transactions);
    } 
    catch (err) {
        console.error(err);
        res.status(500).send({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/transaction/donor/:donorId Get all transactions for a given donor
 * @apiName Transactions
 * @apiGroup Transactions
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *              "id": "20624250-c25b-4071-b120-22796346d564",
 *              "from": "c575c83e-fedb-43e9-8acd-9053c50964cb",
 *              "donorType": "donor",
 *              "to": "cd22cf55-bf43-40bf-8afc-3f48945a9a6b",
 *              "receiverType": "patient",
 *              "quantity": 36.9549,
 *              "comment": "some comment",
 *              "createdAt": "2021-06-30T12:07:45.714Z"
 *          }
 *      ]
 */
transactionRouter.get('/:trxnId', async (req: express.Request, res: express.Response) => {
    try {
        const transaction: Promise<any> = await transactionInstance.getTransaction(req.params.trxnId);
        console.log(transaction);

        if (transaction === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (transaction === null) {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(transaction);
    } 
    catch (err) {
        console.error(err);
        res.status(500).send({ message: errorMessage5XX });
    }
});

/**
 * @api {post} /api/v1/inventory/:bankId Add transaction data
 * @apiName Transactions
 * @apiGroup Transactions
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} from      id of donor
 * @apiParam {String} donorType type of donor
 * @apiParam {String} to        id of receiver
 * @apiParam {String} receiverType type of receiver
 * @apiParam {String} quantity  stringified real value
 * @apiParam {String} comment any comment you'd like to leave
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 You get back an instance of your created inventory 
 *       {
 *           "id": "8359b271-4c85-4fcb-a8b7-239f60955551",
 *           "from": "7359b271-4c85-4fcb-a8b7-239f60955551",
 *           "donorType": "donor",
 *           "to": "4359b271-4c85-4fcb-a8b7-239f60955554",
 *           "receiverType": "patient",
 *           "quantiy": "36.7",
 *           "comment": "comment"   
 *           "createdAt": "2021-07-01T06:57:48.746Z"
 *       }
 */
transactionRouter.post('/', async (req: express.Request, res: express.Response) => {
    try {
        const transaction: Promise<any> = await transactionInstance.addTransaction({...req.body});

        if (transaction === null ||transaction === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        res.status(201).json(transaction);
    } 
    catch (err) {
        console.error(err);
        res.status(500).send({ message: errorMessage5XX });
}
});

export default transactionRouter;
