import express, { NextFunction, Router } from 'express';
import { errorMessage400, errorMessage404, errorMessage5XX } from '../util';
import InventoryService from '../../services/InventoryService';

const inventoryRouter: Router = express.Router();
const inventoryInstance: InventoryService = new InventoryService();

inventoryRouter.use('/', async (req: express.Request, res: express.Response, next: NextFunction) => {
    try {
        await InventoryService.init();
        next();        
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/inventory/all/:bankId Get inventory data for a blood bank
 * @apiName Inventory
 * @apiGroup Inventory
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ResourceNotFoundError) {json} 404 
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *            "id": "9da21b8e-365c-4039-84ae-f42849a1fc9c",
 *            "bloodGroup": "B+",
 *            "donorId": "E86De92ddAe823c",
 *            "district": "Ahmedabad",
 *            "expiryDate": "2020-06-07T00:00:00.000Z",
 *            "bankId": "cd22cf55-bf43-40bf-8afc-3f48945a9a6b",
 *            "status": "status",
 *            "createdAt": "2021-06-30T12:51:05.679Z",
 *            "updatedAt": "2021-06-30T12:51:05.679Z"
 *         }
 *      ]
 */
inventoryRouter.get('/all/:bankId', async (req: express.Request, res: express.Response) => {
    try {
        const items: Promise<any> = await inventoryInstance.getAllItems(req.params.bankId);

        if (items === null || items === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (JSON.stringify(items) === "[]") {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(items);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {post} /api/v1/inventory/:bankId Add inventory data for a blood bank
 * @apiName Inventory
 * @apiGroup Inventory
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} bloodGroup Donor's blood group
 * @apiParam {String} donorId Donor's id   
 * @apiParam {String} expiryDate Mention the expiry date
 * @apiParam {String} bankId Id of the bank which is has this inventory
 * @apiParam {String} status Describe the status of inventory
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 You get back an instance of your created inventory 
 *       {
 *           "id": "8359b271-4c85-4fcb-a8b7-239f60955551",
 *           "bloodGroup": "B+",
 *           "donorId": "F9F4e4a2c90Afd3",
 *           "expiryDate": "2020-06-07T00:00:00.000Z",
 *           "bankId": "c575c83e-fedb-43e9-8acd-9053c50964cb",
 *           "status": "status",
 *           "updatedAt": "2021-07-01T06:57:48.746Z",
 *           "createdAt": "2021-07-01T06:57:48.746Z"
 *       }
 */
inventoryRouter.post('/', async (req: express.Request, res: express.Response) => {
    try {
        const item: Promise<any> = await inventoryInstance.addItem({...req.body});

        if (item === null || item === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }
        
        res.status(201).json(item);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {put} /api/v1/inventory/:itemId Update inventory data
 * @apiName Inventory
 * @apiGroup Inventory
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} [bloodGroup] Donor's blood group
 * @apiParam {String} [donorId] Donor's id 
 * @apiParam {String} [expiryDate] Mention the expiry date
 * @apiParam {String} [status] Describe the status of inventory
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 204 
 * @apiDescription Note that the above parameters are optional; 
 * include only those which you want to be updated.
 */
inventoryRouter.put('/:itemId', async (req: express.Request, res: express.Response) => {
    try {

        if (req.body.bankId !== undefined
            || req.body.itemId !== undefined
            || req.body.createdAt !== undefined
            || req.body.updatedAt !== undefined) {                
                res.status(400).json({ message: errorMessage400 });
                return;                
        }

        const item: Promise<any> = await inventoryInstance.updateItem(req.params.itemId, {...req.body});
        let ob: string = JSON.stringify(item);

        if (ob === '[0]') {
            res.status(404).json({ message: errorMessage404 });
            return;                
        }

        res.status(204).json(item);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {delete} /api/v1/inventory/:itemId Delete inventory by id
 * @apiName Inventory
 * @apiGroup Inventory
 * @apiError (ResourceNotFoundError) {json} 404
 * @apiError ServerError {json} 500 
 * @apiParam {String} itemId id of the item you want to delete.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *     {
 *         "message": "Deleted"
 *     }
 */
inventoryRouter.delete('/:itemId', async (req: express.Request, res: express.Response) => {
    try {
        const item: Promise<any> = await inventoryInstance.deleteItem(req.params.itemId);
        let ob: string = JSON.stringify(item);

        if (ob === '0') {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json({ message: 'Deleted'});
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

export default inventoryRouter;
