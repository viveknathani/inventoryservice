import express, { NextFunction, Router } from 'express';
import { errorMessage5XX, errorMessage404, errorMessage400 } from '../util';
import BankService from '../../services/BankService';

const bankRouter: Router = express.Router();
const bankInstance: BankService = new BankService();

bankRouter.use('/', async (req: express.Request, res: express.Response, next: NextFunction) => {
    try {
        await BankService.init();
        next();        
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/bank/ Get all blood banks
 * @apiName Banks
 * @apiGroup Banks
 * @apiError (ServerError) {json} 500 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *            "id": "9da21b8e-365c-4039-84ae-f42849a1fc9c",
 *            "name": "name",
 *            "address": "403 Shakti Appts",
 *            "district": "Ahmedabad",
 *            "state": "Gujarat",
 *            "pincode": "380006",
 *            "mobileNumber": "1308335963",
 *            "email": "bT8UHsW5jpoJbSbhENaS9hQEU@gmail.com",
 *            "ownerId": "kyfEVYx7hJYLfmT",
 *            "createdAt": "2021-06-30T12:51:05.679Z",
 *            "updatedAt": "2021-06-30T12:51:05.679Z"
 *         }
 *      ]
 */
bankRouter.get('/', async (req: express.Request, res: express.Response) => {
    try {
        const banks: Promise<any> = await bankInstance.getAllBanks();
        res.status(200).json(banks);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {get} /api/v1/bank/:bankId Get a blood bank by id
 * @apiName Banks
 * @apiGroup Banks
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ResourceNotFoundError) {json} 404
 * @apiError ServerError {json} 500 
 * @apiParam {String} bankId id of the bank you need.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *      [
 *         {
 *            "id": "9da21b8e-365c-4039-84ae-f42849a1fc9c",
 *            "name": "name",
 *            "address": "403 Shakti Appts",
 *            "district": "Ahmedabad",
 *            "state": "Gujarat",
 *            "pincode": "380006",
 *            "mobileNumber": "1308335963",
 *            "email": "bT8UHsW5jpoJbSbhENaS9hQEU@gmail.com",
 *            "ownerId": "kyfEVYx7hJYLfmT",
 *            "createdAt": "2021-06-30T12:51:05.679Z",
 *            "updatedAt": "2021-06-30T12:51:05.679Z"
 *         }
 *      ]
 */
bankRouter.get('/:bankId', async (req: express.Request, res: express.Response) => {
    try {
        const bank: Promise<any> = await bankInstance.getBank(req.params.bankId);

        if (bank === null || bank === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        if (JSON.stringify(bank) === "[]") {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json(bank);
    }
    catch (err) {   
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {post} /api/v1/bank/ Add a blood bank
 * @apiName Banks
 * @apiGroup Banks
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} name Name of the bank
 * @apiParam {String} address Address of the bank
 * @apiParam {String} district District of the bank
 * @apiParam {String} pincode Stringified pincode of the bank
 * @apiParam {String} mobileNumber Stringified mobile number of the bank
 * @apiParam {String} email Email-id of the bank
 * @apiParam {String} ownerId id of the owner of the bank
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 You get back an instance of your created bank 
 *     {
 *       "id": "9da21b8e-365c-4039-84ae-f42849a1fc9c",
 *       "name": "name",
 *       "address": "403 Shakti Appts",
 *       "district": "Ahmedabad",
 *       "state": "Gujarat",
 *       "pincode": 380006,
 *       "mobileNumber": "1308335963",
 *       "email": "bT8UHsW5jpoJbSbhENaS9hQEU@gmail.com",
 *       "ownerId": "kyfEVYx7hJYLfmT",
 *       "createdAt": "2021-06-30T12:51:05.679Z",
 *       "updatedAt": "2021-06-30T12:51:05.679Z"
 *     }
 */
bankRouter.post('/', async (req: express.Request, res: express.Response) => {
    try {
        const bank: Promise<any> = await bankInstance.addBank({...req.body});
        
        if (bank === null || bank === undefined) {
            res.status(400).json({ message: errorMessage400 });
            return;
        }

        res.status(201).json(bank);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {put} /api/v1/bank/ Update a blood bank
 * @apiName Banks
 * @apiGroup Banks
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {String} [name] Name of the bank
 * @apiParam {String} [address] Address of the bank
 * @apiParam {String} [district] District of the bank
 * @apiParam {String} [pincode] Stringified pincode of the bank
 * @apiParam {String} [mobileNumber] Stringified mobile number of the bank
 * @apiParam {String} [email] Email-id of the bank
 * @apiParam {String} [ownerId] id of the owner of the bank
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 204 
 * @apiDescription Note that the above parameters are optional; 
 * include only those which you want to be updated.
 */
bankRouter.put('/:bankId', async (req: express.Request, res: express.Response) => {
    try {

        if (req.body.bankId !== undefined
            || req.body.createdAt !== undefined
            || req.body.updatedAt !== undefined) {                
            res.status(400).json({ message: errorMessage400 });
            return;                
        }

        const bank: Promise<any> = await bankInstance.updateBank(req.params.bankId, {...req.body});
        let ob: string = JSON.stringify(bank);

        if (ob === '[0]') {
            res.status(404).json({ message: errorMessage404 });
            return;                
        }

        res.status(204).json(bank);
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

/**
 * @api {delete} /api/v1/bank/:bankId Delete a blood bank by id
 * @apiName Banks
 * @apiGroup Banks
 * @apiError (IncorrectRequestError) {json} 400
 * @apiError (ServerError) {json} 500 
 * @apiParam {string} :bankId id of the bank you want to delete   
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200
 *     {
 *         "message": "Deleted"
 *     }
 */
bankRouter.delete('/:bankId', async (req: express.Request, res: express.Response) => {
    try {
        const bank: Promise<any> = await bankInstance.deleteBank(req.params.bankId);
        let ob: string = JSON.stringify(bank);

        if (ob === '0') {
            res.status(404).json({ message: errorMessage404 });
            return;
        }

        res.status(200).json({ message: 'Deleted'});
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ message: errorMessage5XX });
    }
});

export default bankRouter;
