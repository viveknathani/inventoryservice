/**
 * An object is exported which contains all the environment variables.
 * Note: This object should be accessed only after dotenv.config() is 
 * called. (This is done in ../app.ts)
 */

export default {
    PORT: process.env.PORT || 4000,
    database: {
        host: process.env.DATABASE_HOST || 'localhost',
        name: process.env.DATABASE_NAME || 'inventory',
        username: process.env.DATABASE_USER_NAME || 'postgres',
        password: process.env.DATABASE_PASSWORD || 'root'
    }
}
