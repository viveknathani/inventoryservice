interface BankAttributes {
    name?: string, 
    address?: string, 
    district?: string, 
    state?: string, 
    pincode?: number, 
    mobileNumber?: number, 
    email?: string, 
    ownerId?: string    
}

interface InventoryAttributes {
    bloodGroup?: string,
    donorId?: string,
    expiryDate?: Date,
    bankId?: string,
    status?: string
}

interface TransactionAttributes {
    from?: string,
    donorType?: string,
    to?: string,
    quantiy?: number,
    comment?: string
}

export {
    BankAttributes,
    InventoryAttributes,
    TransactionAttributes
}
