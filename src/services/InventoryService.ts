import { Model, ModelCtor, Op } from 'sequelize';
import { BankAttributes, InventoryAttributes } from './interfaces';
import DataLayer from './DataLayer';

/**
 * Contains methods to interact with the InventoryModel as per
 * business requirements.
 */
class InventoryService extends DataLayer {

    constructor() {
        super();
    }

    /**
     * @returns The InventoryModel, to be used internally by other methods.
     */
     private getModel(): ModelCtor<Model<any, any>> {
        return InventoryService.data.getInventoryModel();
    }

    /**
     * Fetch all inventory data for a given bank from the database.
     * @param bankId 
     */
    async getAllItems(bankId: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const inventory = await model.findAll({
                where: {
                    bankId: bankId
                }
            });
            return inventory;
        } 
        catch(err) {
            console.error(err);
        }
    }

    /**
     * Add inventory data to the database.
     * @param inventoryObject An object of type InventoryAttributes, 
     *                        should contain all the attributes
     *                        except itemId.
     * @returns               The created instance of the model. 
     */
    async addItem(inventoryObject: InventoryAttributes): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const inventory = await model.create({...inventoryObject});
            return inventory;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Update a given item's details.
     * @param itemId          The itemId to identify with.
     * @param inventoryObject The object that should contain the attributes which are updated.
     * @returns               The promise returns an array with one or two elements. 
     *                        The first element is always the number of affected rows, 
     *                        while the second element is the actual affected rows   
     */
     async updateItem(itemId: string, inventoryObject: InventoryAttributes): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const inventory = await model.update({...inventoryObject}, {
                where: {
                    id: itemId
                }
            })
            return inventory;
        }
        catch (err) {
            console.error(err);
        }
    }


    /**
     * Delete a given item in the database.
     * @param itemId The itemId to identify with.
     * @returns      The number of destroyed rows as a promise.
     */
     async deleteItem(itemId: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const inventory = model.destroy({
                where: {
                    id: itemId
                }
            });
            return inventory;
        }
        catch (err) {
            console.error(err);
        }
    }
    
    /**
     * Find all banks which have the specified properties and the specified inventory data too.
     * @param inventoryObject The object that should contain the inventory attributes which are needed.
     * @param bankObject      The object that should contain the bank which are updated.
     * @returns 
     */
    async findWithTwoSets(inventoryObject: InventoryAttributes, 
                          bankObject: BankAttributes): Promise<any> {
        
        const inventoryModel: ModelCtor<Model<any, any>> = this.getModel()
        const bankModel: ModelCtor<Model<any, any>> = InventoryService.data.getBankModel();
        
        try {
            const data = await bankModel.findAll({
                include: [{
                    model: inventoryModel,
                    where: {...inventoryObject},
                    required: true
                }],
                where: {
                    ...bankObject
                },
            });
            return data;
        }
        catch (err) {
            console.error(err);
        }
    }
}

export default InventoryService;
