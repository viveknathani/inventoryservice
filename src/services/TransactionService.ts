import { Model, ModelCtor, Op } from 'sequelize';
import DataLayer from './DataLayer';
import { TransactionAttributes } from './interfaces';

/**
 * Contains methods to interact with the TransactionModel as per
 * business requirements.
 */
class TransactionService extends DataLayer {

    constructor() {
        super();
    }

    /**
     * @returns The InventoryModel, to be used internally by other methods.
     */
     private getModel(): ModelCtor<Model<any, any>> {
        return TransactionService.data.getTransactionModel();
    }

    /**
     * Fetch all transactions from the database.
     * @returns All transactions
     */
    async getAllTransactions(): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const transactions = await model.findAll();
            return transactions;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Returns a list of transactions which involve a given bank/donor.
     * @param b
     * @returns      The list of transactions.
     */
    async getTransactions(type: string, id: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();

            // To know more about usage of 'Op',
            // check: [https://sequelize.org/master/manual/model-querying-basics.html#operators]
            const transactions = await model.findAll({
                where: {
                    [Op.and]: [ {donorType: type}, 
                                { [Op.or]: [{ from: id }, { to: id }] } ]
                }
            });
            return transactions;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Get details of a single transaction
     * @param transactionId The id of the transaction
     * @returns             The object that contains the details you need.
     */
    async getTransaction(transactionId: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const transaction = await model.findOne({
                where: {
                    id: transactionId
                }
            });
            return transaction;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Add a transaction to the database.
     * @param transactionObject The object with all the fields filled.
     * @returns                 Created instance of the model.
     */
    async addTransaction(transactionObject: TransactionAttributes): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const transaction = await model.create({...transactionObject});
            return transaction;
        }
        catch (err) {
            console.error(err);
        }
    }
}

export default TransactionService;
