import { Model, ModelCtor } from 'sequelize';
import DataLayer from './DataLayer';
import { BankAttributes } from './interfaces';

/**
 * Contains methods to interact with the BankModel as per
 * business requirements.
 */
class BankService extends DataLayer {

    constructor() {
        super();
    }

    /**
     * @returns The BankModel, to be used internally by other methods.
     */
    private getModel(): ModelCtor<Model<any, any>> {
        return BankService.data.getBankModel(); 
    }

    /**
     * Fetch all banks from the database.
     * @returns A list of banks. 
     */
    async getAllBanks(): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const banks = await model.findAll();
            return banks;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Fetch the bank you need.
     * @param bankId The UUID string of the bank.
     * @returns      A list of size 0 or 1. 
     */
    async getBank(bankId: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const bank = await model.findAll({
                where: {
                    id: bankId   
                }
            });
            return bank;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Create a bank in the database.
     * @param bankObject An object of type BankAttributes, 
     *                   should contain all the attributes
     *                   except bankId.
     * @returns          The created instance of the model.
     */
    async addBank(bankObject: BankAttributes): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const bank = await model.create({...bankObject});
            return bank;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Update a given bank's details.
     * @param bankId     The bankId to identify with.
     * @param bankObject The object that should contain the attributes which are updated.
     * @returns          The promise returns an array with one or two elements. 
     *                   The first element is always the number of affected rows, 
     *                   while the second element is the actual affected rows   
     */
    async updateBank(bankId: string, bankObject: BankAttributes): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const bank = await model.update({...bankObject}, {
                where: {
                    id: bankId
                }
            })
            return bank;
        }
        catch (err) {
            console.error(err);
        }
    }

    /**
     * Delete a given bank in the database.
     * @param bankId The bankId to identify with.
     * @returns      The number of destroyed rows as a promise.
     */
    async deleteBank(bankId: string): Promise<any> {
        try {
            const model: ModelCtor<Model<any, any>> = this.getModel();
            const bank = await model.destroy({
                where: {
                    id: bankId
                }
            });
            return bank;
        }
        catch (err) {
            console.error(err);
        }
    }
}

export default BankService;
