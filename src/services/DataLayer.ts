import SQL from '../models/SQL';

class DataLayer {
    protected static data = new SQL();

    static async init(): Promise<any> {

        if (!DataLayer.data.isConnectionSet()) {
            try {
                await DataLayer.data.setupConnection();
            }
            catch (err) {
                console.error(err);
            }
        }
    }

    static async kill(): Promise<any> {
        try {
            await DataLayer.data.closeConnection();
        }
        catch(err) {
            console.error(err);
        }
    }
}

export default DataLayer;
